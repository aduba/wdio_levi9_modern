import { DropdownSelectors } from './fragments';

export class Models extends DropdownSelectors {
    constructor(){
        super()
    }
    auris(){
        const auris = this.aurisCheckbox;
            browser.waitUntil(
                () => auris.isDisplayed(),
                {
                    timeout: 3000,
                    timeoutMsg: 'Auris is not displayed'
                }
            )
        auris.click()
    }
    mondeo(){
        const mondeo = this.mondeoCheckbox;
            browser.waitUntil(
                () => mondeo.isDisplayed(),
                {
                    timeout: 3000,
                    timeoutMsg: 'Mondeo is not displayed'
                }
            )
        mondeo.click();
    }
    passat(){
        const passat = this.passatCheckbox;
            browser.waitUntil(
                () => passat.isDisplayed(),
                {
                    timeout: 3000,
                    timeoutMsg: 'Mondeo is not displayed'
                }
            )
        passat.click();
    }
}
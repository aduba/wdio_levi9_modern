import { DropdownSelectors } from './fragments';

export class Brands extends DropdownSelectors {
    constructor(){
        super()
    }
    selectModel(modelCheckbox){
        modelCheckbox.scrollIntoView()
        modelCheckbox.click()
    }
}
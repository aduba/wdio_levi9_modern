import { DropdownSelectors } from './fragments';

export class Selection extends DropdownSelectors {
    constructor(){
        super()
    }
    makeDropdown(){
        const makeDropdown = this.makeDropdownSelector;
            browser.waitUntil(function(){
                return makeDropdown.isDisplayed()
            }, 3000, 'Make dropdown is not displayed')
        makeDropdown.click()
    }
    modelDropdown(){
        const modelDropdown = this.modelDropdownSelector;
            browser.waitUntil(
                () => modelDropdown.isDisplayed(),
                {
                    timeout: 3000,
                    timeoutMsg: 'Model dropdown is not displayed'
                }
            )
        modelDropdown.click()
    }
    search(){
        const search = this.searchButton;
            browser.waitUntil(
                () => search.isDisplayedInViewport(),
                {
                    timeout: 3000,
                    timeoutMsg: 'Search button is not displayed in viewport'
                }
            )
        search.click()
    }
}
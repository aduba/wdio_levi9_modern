export class DropdownSelectors {

    get makeDropdownSelector() {return $('div[data-e2e-id="brandDropdown"]')};
    get modelDropdownSelector() {return $('div[data-e2e-id="modelDropdown"]')};
    get searchButton() {return $('button[data-e2e-id="searchButton"]')};

    get toyotaCheckbox() {return $('input[value="toyota"] + div')};
    get aurisCheckbox() {return $('input[value="auris"] + div')};
    
    get fordCheckbox() {return $('input[value="ford"] + div')};
    get mondeoCheckbox() {return $('input[value="mondeo"] + div')};

    get vwCheckbox() {return $('input[value="volkswagen"] + div')};
    get passatCheckbox() {return $('input[value="passat"] + div')};

}
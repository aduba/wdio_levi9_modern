export class FilterSelectors {

    get priceMinSelector() {return $('#salePriceFrom')};
    get priceMaxSelector() {return $('#salePriceTo')};

    get transmissionAutoSelector() {return $('input[value="automatic"] + div')};
    get transmissionManSelector() {return $('input[value="manual"] + div')};

    get sortingSelector() {return $('div.GridItem-sc-zx88b1.chhrEs div[data-e2e-id="sortingDropdown"]')};
    get sortingPriceAscSelector() {return $('div.GridItem-sc-zx88b1.chhrEs option[value="priceAsc"]')};

    get minValue7500Selector() {return this.priceMinSelector.$('select[data-e2e-select-input] option[value="7500"]')};
    get minValue10000Selector() {return this.priceMinSelector.$('select[data-e2e-select-input] option[value="10000"]')};
    get minValue15000Selector() {return this.priceMinSelector.$('select[data-e2e-select-input] option[value="15000"]')};
    get minValue20000Selector() {return this.priceMinSelector.$('select[data-e2e-select-input] option[value="20000"]')};
    get minValue25000Selector() {return this.priceMinSelector.$('select[data-e2e-select-input] option[value="25000"]')};

    get maxValue7500Selector() {return this.priceMaxSelector.$('select[data-e2e-select-input] option[value="7500"]')};
    get maxValue10000Selector() {return this.priceMaxSelector.$('select[data-e2e-select-input] option[value="10000"]')};
    get maxValue15000Selector() {return this.priceMaxSelector.$('select[data-e2e-select-input] option[value="15000"]')};
    get maxValue20000Selector() {return this.priceMaxSelector.$('select[data-e2e-select-input] option[value="20000"]')};
    get maxValue25000Selector() {return this.priceMaxSelector.$('select[data-e2e-select-input] option[value="25000"]')};

}
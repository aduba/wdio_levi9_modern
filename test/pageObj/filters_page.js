import { FilterSelectors } from './fragments'

export class Filters extends FilterSelectors {
    constructor(){
        super()
    }
    priceMinSelect(){
        const priceMinSelect = this.priceMinSelector
        priceMinSelect.click()
    }
    priceMaxSelect(){
        const priceMaxSelect = this.priceMaxSelector
        priceMaxSelect.click()
    }
    transmissionAutoSelect(){
        const transmissionAutoSelect = this.transmissionAutoSelector
        transmissionAutoSelect.click()
    }
    transmissionManSelect(){
        const transmissionManSelect = this.transmissionManSelector
        transmissionManSelect.click()
    }
    sortingSelect(){
        const sortingSelect = this.sortingSelector
        sortingSelect.click()
    }
    sortingPriceAscSelect(){
        const sortingPriceAscSelect = this.sortingPriceAscSelector
        sortingPriceAscSelect.click()
    }
}
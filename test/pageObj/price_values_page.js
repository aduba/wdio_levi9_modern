import { FilterSelectors } from './fragments'

export class PriceValues extends FilterSelectors{
    constructor(){
        super()
    }
    minValue7500Select(){
        const minValue7500Select = this.minValue7500Selector
        minValue7500Select.click()
    }
    minValue10000Select(){
        const minValue10000Select = this.minValue10000Selector
        minValue10000Select.click()
    }
    minValue15000Select(){
        const minValue15000Select = this.minValue15000Selector
        minValue15000Select.click()
    }
    minValue20000Select(){
        const minValue20000Select = this.minValue20000Selector
        minValue20000Select.click()
    }

    maxValue10000Select(){
        const maxValue10000Select = this.maxValue10000Selector
        maxValue10000Select.click()
    }
    maxValue15000Select(){
        const maxValue15000Select = this.maxValue15000Selector
        maxValue15000Select.click()
    }
    maxValue20000Select(){
        const maxValue20000Select = this.maxValue20000Selector
        maxValue20000Select.click()
    }
    maxValue25000Select(){
        const maxValue25000Select = this.maxValue25000Selector
        maxValue25000Select.click()
    }
}

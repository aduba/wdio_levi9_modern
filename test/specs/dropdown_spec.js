import { expect } from 'chai'
import { Selection, Brands, Models } from '../pageObj'
import { DropdownSelectors } from '../pageObj';

describe('Dropdown selection checking', function(){
    const selection = new Selection()
    const models = new Models()
    const brands = new Brands()
    const dropdownSelectors = new DropdownSelectors()

    it('Should check make and model vw dropdown selection', function(){
        browser.url('/en-be')
        selection.makeDropdown()
        brands.selectModel(dropdownSelectors.vwCheckbox)
        selection.modelDropdown()
        models.passat()
        selection.search()     
        const currentUrl = browser.getUrl()
        expect(currentUrl).to.equal('https://www.carnext.com/en-be/cars/volkswagen/passat/', 'Incorrect vw sorting applied')
    })
    it('Should check make and model ford dropdown selection', function(){
        browser.url('/en-be')
        selection.makeDropdown()
        brands.selectModel(dropdownSelectors.fordCheckbox)
        selection.modelDropdown()
        models.mondeo()
        selection.search()     
        const currentUrl = browser.getUrl()
        expect(currentUrl).to.equal('https://www.carnext.com/en-be/cars/ford/mondeo/', 'Incorrect ford sorting applied')
    })
    it('Should check make and model toyota dropdown selection', function(){
        browser.url('/en-be')
        selection.makeDropdown()
        brands.selectModel(dropdownSelectors.toyotaCheckbox)
        selection.modelDropdown()
        models.auris()
        selection.search()     
        const currentUrl = browser.getUrl()
        expect(currentUrl).to.equal('https://www.carnext.com/en-be/cars/toyota/auris/', 'Incorrect toyota sorting applied')
    })
})
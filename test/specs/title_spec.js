import { expect } from 'chai'

describe('Carnext page title', function (){
    it('should display correct title', function(){
        browser.url('/en-be')
        const title = browser.getTitle()
        expect(title).to.be.equal('Used cars | Belgium | CarNext.com', 'Incorrect title, recheck!')
    })
})